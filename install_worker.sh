#!/bin/bash

echo "[TASK 1] Install containerd runtime"
# Installer Containerd
## Prérequis
modprobe overlay
modprobe br_netfilter
sudo swapoff -a
sudo apt install apt-transport-https curl
## Installation de Containerd
apt-get update && apt-get install -y containerd

## Configurer Containerd
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml

## Redémarrer Containerd
systemctl restart containerd

echo "[TASK 2] Install Kubernetes components (kubeadm, kubelet and kubectl)"

# Installer kubeadm, kubelet et kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update
apt-get install -y kubeadm=1.22.0-00 kubelet=1.22.0-00 kubectl=1.22.0-00 
