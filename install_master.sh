#!/bin/bash

echo "[TASK 1] Install containerd runtime"
# Installer Containerd
## Prérequis
modprobe overlay
modprobe br_netfilter

## Installation de Containerd
apt-get update && apt-get install -y containerd

## Configurer Containerd
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml

## Redémarrer Containerd
systemctl restart containerd

echo "[TASK 2] Install Kubernetes components (kubeadm, kubelet and kubectl)"

# Installer kubeadm, kubelet et kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update
apt-get install -y kubeadm=1.26.0-00 kubelet=1.26.0-00 kubectl=1.26.0-00 

echo "[TASK 3] Pull required containers"

kubeadm config images pull >/dev/null 2>&1

echo "[TASK 4] Initialize Kubernetes Cluster"
# Initialiser le cluster
kubeadm init --pod-network-cidr=10.244.0.0/16 --cri-socket=/run/containerd/containerd.sock  --ignore-preflight-errors=all >> /root/kubeinit.log 2>&1


 echo "[TASK 5] Copy kube admin config to root user .kube directory"
# Configurer kubectl pour l'utilisateur actuel
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

echo "[TASK 6] Deploy Flannel network"
# Installer le réseau de pod (ici, flannel)

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml > /dev/null 2>&1


echo "[TASK 7] Generate and save cluster join command to /joincluster.sh"
# Rejoindre le cluster avec la commande "join"

  joinCommand=$(kubeadm token create --print-join-command 2>/dev/null)
  echo "$joinCommand --ignore-preflight-errors=all" > /joincluster.sh
